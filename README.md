# SODEE Model Trainer #

A simple Java program, which imports data from a sqlite3 database and train/evaluate WEKA machine learning models.

## Start the model trainer ##
Start the built jar (don't forget the dependencies) with the following command and options
```
java -jar SODEEModelTrainer.jar "PATH TO SQLITE DB" "PATH TO A DIRECTORY FOR THE OUTPUT" "A real number for the train/test-ratio"
```
Example with 0.75 ratio of training and test data
```
java -jar SODEEModelTrainer.jar "/media/Data/sodee.db" "/media/Data/TrainerOutput/" "0.75"
```
## Dependencies ##
* Weka
* Database Driver for SQLite3