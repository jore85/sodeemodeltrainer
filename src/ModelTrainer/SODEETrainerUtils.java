/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelTrainer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.core.Instances;

/**
 *
 * @author jore
 */
public class SODEETrainerUtils {
    
    public static double calculateLIFTMetric(double tpr, double fpr, int trues, int negatives) {
        double liftMetric = 0;
        double x = tpr + fpr;
        double y = trues + negatives;
        liftMetric = x / y;
        return liftMetric;
    }
    
    public static String generateROCCurve(Instances dataPoints){ 
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < dataPoints.numInstances(); i++){
           double fpr = dataPoints.instance(i).value(4);
           double tpr = dataPoints.instance(i).value(5);
           sb.append(fpr).append(",").append(tpr).append("\n");           
        }
        return sb.toString();
    }
    
    public static String generateLIFTCurve(Instances dataPoints, int trues, int negatives){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < dataPoints.numInstances(); i++){
            double tpr = dataPoints.instance(i).value(4);            
            double fpr = dataPoints.instance(i).value(5);
            sb.append(calculateLIFTMetric(tpr, fpr, trues, negatives)).append(",").append(tpr).append("\n");            
        }
        return sb.toString();
    }
    
    public static int[] calcNumberInstancesOfClass(Instances data){
        int numInstances = data.numInstances();
        int[] result = new int[2]; // class "positive": [0] ; class "negative": [1]
        result[0] = 0;
        result[1] = 0;
        for(int i = 0; i < numInstances; i++){
            double classValue = data.get(i).classValue();
            if(classValue == 1){
                result[0]++;
            } else {
                result[1]++;
            }
        }
        return result;
    }
    
    public static void writeTXTFile(String data, String path){
        try {
            FileWriter fw = new FileWriter(new File(path));
            fw.write(data);
            fw.flush();
            fw.close();
        } catch (IOException ex) {
            System.out.println("No file was written. Some error occurs.");
        }
    }
    
    public static void exportModel(String pathToModelFile, Classifier model){
        try {
            ObjectOutputStream oos = null;
            oos = new ObjectOutputStream(new FileOutputStream(pathToModelFile));
            oos.writeObject(model);
            oos.flush();
            oos.close();
        } catch (IOException ex) {
            Logger.getLogger(SODEETrainerUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
