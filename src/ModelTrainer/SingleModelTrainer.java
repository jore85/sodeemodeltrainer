/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelTrainer;

import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.ThresholdCurve;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;

/**
 *
 * @author jore
 */
public class SingleModelTrainer implements Runnable {

    private final Instances trainingData;
    private final Instances testData;

    private final String name;
    private final String description;
    private final String modelOutputPath;
    private final String evaluationFilesOutputPath;
    private final String evalOutputFile;
    private final String modelOutputFile;
    private final String logFile;
    private final String ROCDataFile;

    private final FilteredClassifier model;

    public SingleModelTrainer(Instances trainingData, Instances testData, String name,
            String description, String outputPath, FilteredClassifier model) {
        this.trainingData = trainingData;
        this.testData = testData;
        this.name = name;
        this.description = description;
        this.modelOutputPath = outputPath;
        this.evaluationFilesOutputPath = outputPath;
        this.model = model;
        this.evalOutputFile = this.evaluationFilesOutputPath + this.name + "_eval.txt";
        this.logFile = this.evaluationFilesOutputPath + this.name + ".log";
        this.modelOutputFile = this.modelOutputPath + this.name + ".model";
        this.ROCDataFile = this.evaluationFilesOutputPath + this.name + "_ROCData.csv";
    }

    @Override
    public void run() {
        StringBuilder sbLog = new StringBuilder();
        StringBuilder evalString = new StringBuilder();
        evalString.append(this.description).append("\n");

        //Training
        sbLog.append("Train model: ").append(this.name).append("...\t").append(System.currentTimeMillis()).append("\n");
        //System.out.println("Train model: " + this.name + "...\t" + System.currentTimeMillis());
        try {
            this.model.buildClassifier(this.trainingData);
        } catch (Exception ex) {
            Logger.getLogger(SingleModelTrainer.class.getName()).log(Level.SEVERE, null, ex);
        }
        sbLog.append("Model: ").append(this.name).append(" trained\t").append(System.currentTimeMillis()).append("\n");
        //System.out.println("Model: " + this.name + " trained\t" + System.currentTimeMillis());

        //Model Export
        sbLog.append("Export trained model: ").append(this.name).append("...\t").append(System.currentTimeMillis()).append("\n");
        //System.out.println("Export trained model: " + this.name + "...\t" + System.currentTimeMillis());
        SODEETrainerUtils.exportModel(this.modelOutputFile, this.model);
        sbLog.append("Model: ").append(this.name).append(" exported\t").append(System.currentTimeMillis()).append("\n");
        //System.out.println("Model: " + this.name + " exported\t" + System.currentTimeMillis());

        //Evaluation
        sbLog.append("Evaluate model: ").append(this.name).append("...\t").append(System.currentTimeMillis()).append("\n");
        //System.out.println("Evaluate model: " + this.name + "...\t" + System.currentTimeMillis());
        try {
            Evaluation eval = new Evaluation(trainingData);
            eval.evaluateModel(this.model, testData);
            evalString.append("\n").append(eval.toSummaryString());
            evalString.append(eval.toClassDetailsString()).append("\n");
            evalString.append(eval.toMatrixString()).append("\n");
            ThresholdCurve tc = new ThresholdCurve();
            Instances curve = tc.getCurve(eval.predictions());
            String rocCurveValues = SODEETrainerUtils.generateROCCurve(curve);
            SODEETrainerUtils.writeTXTFile(rocCurveValues, this.ROCDataFile);
            sbLog.append("Model: ").append(this.name).append(" evaluated\t").append(System.currentTimeMillis()).append("\n");
            // System.out.println("Model: " + this.name + " evaluated\t" + System.currentTimeMillis());
        } catch (Exception ex) {
            Logger.getLogger(SingleModelTrainer.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Write Data to File
        SODEETrainerUtils.writeTXTFile(evalString.toString(), this.evalOutputFile);
        SODEETrainerUtils.writeTXTFile(sbLog.toString(), this.logFile);
    }

}
