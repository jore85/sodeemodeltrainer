/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package ModelTrainer;

// JAVA DOC: http://weka.sourceforge.net/doc.stable-3-8/
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.CorrelationAttributeEval;
import weka.attributeSelection.GainRatioAttributeEval;
import weka.attributeSelection.GreedyStepwise;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.*;
import weka.filters.unsupervised.attribute.Remove;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.supportVector.Puk;
import weka.classifiers.meta.AdaBoostM1;
import weka.classifiers.meta.WeightedInstancesHandlerWrapper;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Utils;
import weka.core.converters.ArffSaver;

/**
 *
 * @author johan
 */
public class WekaTest {

    public static void main(String[] args) {

        String sqliteDB = args[0];
        String outputPath = args[1];
        String trainTestSplit = args[2];
        Float trainTestSplitValue = Float.parseFloat(trainTestSplit);

        StringBuilder sb = new StringBuilder();

        ArrayList<String> neClassNominalValues = new ArrayList<>(4);
        neClassNominalValues.add("PERSON");
        neClassNominalValues.add("ORGANIZATION");
        neClassNominalValues.add("LOCATION");
        neClassNominalValues.add("MISC");

        ArrayList<String> posTags = new ArrayList<>(7);
        posTags.add("NN");
        posTags.add("NNP");
        posTags.add("NNPS");
        posTags.add("NNS");
        posTags.add("JJS");
        posTags.add("JDI");
        posTags.add("JJ");
        posTags.add("");
        posTags.add("CD");
        posTags.add("$");
        posTags.add("WDT");

        ArrayList<String> classValues = new ArrayList<>(2);
        classValues.add("positive");
        classValues.add("negative");

        ArrayList<String> booleanNominal = new ArrayList<>(2);
        booleanNominal.add("true");
        booleanNominal.add("false");

        ArrayList<String> dateCountsNominal = new ArrayList<>(24);
        dateCountsNominal.add("0");
        dateCountsNominal.add("1");
        dateCountsNominal.add("2");
        dateCountsNominal.add("3");
        dateCountsNominal.add("4");
        dateCountsNominal.add("5");
        dateCountsNominal.add("6");
        dateCountsNominal.add("7");
        dateCountsNominal.add("8");
        dateCountsNominal.add("9");
        dateCountsNominal.add("10");
        dateCountsNominal.add("11");
        dateCountsNominal.add("12");
        dateCountsNominal.add("13");
        dateCountsNominal.add("14");
        dateCountsNominal.add("15");
        dateCountsNominal.add("16");
        dateCountsNominal.add("17");
        dateCountsNominal.add("18");
        dateCountsNominal.add("19");
        dateCountsNominal.add("20");
        dateCountsNominal.add("21");
        dateCountsNominal.add("22");
        dateCountsNominal.add("23");

        ArrayList<Attribute> sodeeFeatures = new ArrayList<>(21);
        sodeeFeatures.add(new Attribute("rowID"));
        sodeeFeatures.add(new Attribute("NP", (List<String>) null));
        sodeeFeatures.add(new Attribute("Named_Entity_Class", neClassNominalValues));
        sodeeFeatures.add(new Attribute("Pos", posTags));
        sodeeFeatures.add(new Attribute("hasRedLink", booleanNominal));
        sodeeFeatures.add(new Attribute("titleContainsNP", booleanNominal));
        sodeeFeatures.add(new Attribute("firstCapital", booleanNominal));
        sodeeFeatures.add(new Attribute("isAllCaps", booleanNominal));
        sodeeFeatures.add(new Attribute("dayOfWeek", dateCountsNominal));
        sodeeFeatures.add(new Attribute("lastFullHour", dateCountsNominal));
        sodeeFeatures.add(new Attribute("pageView24hSum"));
        sodeeFeatures.add(new Attribute("pageViewSlope24hSlope"));
        sodeeFeatures.add(new Attribute("pageViewSlope24hIntercept"));
        sodeeFeatures.add(new Attribute("npOccurrenceNo1h"));
        sodeeFeatures.add(new Attribute("npOccurrenceNo24h"));
        sodeeFeatures.add(new Attribute("npOccurrenceSlope24hSlope"));
        sodeeFeatures.add(new Attribute("npOccurrenceSlope24hIntercept"));
        sodeeFeatures.add(new Attribute("googleNgramFrequency"));
        sodeeFeatures.add(new Attribute("npPositionInArticle"));
        sodeeFeatures.add(new Attribute("articleCoverage"));
        sodeeFeatures.add(new Attribute("class", classValues));

        Instances fullDataSet = new Instances("SODEE01", sodeeFeatures, 0);
        fullDataSet.setClassIndex(fullDataSet.numAttributes() - 1);

        String SQLITE_DB_URL = "jdbc:sqlite:" + sqliteDB;

        double trues = 0;
        double falses = 0;
        Connection conn = null;
        try {
            Class.forName("org.sqlite.JDBC").newInstance();
            conn = DriverManager.getConnection(SQLITE_DB_URL);
            String stmntString = "SELECT rowID, NP, Named_Entity_Class, Pos, hasRedLink, "
                    + "titleContainsNP, firstCapital, isAllCaps, dayOfWeek, lastFullHour, pageView24hSum , pageViewSlope24hSlope, pageViewSlope24hIntercept,"
                    + "npOccurrenceNo1h, npOccurrenceNo24h, npOccurrenceSlope24hSlope, npOccurrenceSlope24hIntercept, googleNgramFrequency, npPositionInArticle, "
                    + "articleCoverage, class FROM allNPFeatures WHERE (class = 1) AND ((NPTimestamp BETWEEN  strftime('%s', '2015-04-24') AND  strftime('%s', '2015-04-25'))\n"
                    + "OR (NPTimestamp BETWEEN  strftime('%s', '2015-04-26') AND  strftime('%s', '2015-05-04')))";//ORDER BY RANDOM() LIMIT 200";
            Statement createStatement = conn.createStatement();
            ResultSet executeQuery = createStatement.executeQuery(stmntString);
            while (executeQuery.next()) {
                Instance row = new DenseInstance(fullDataSet.numAttributes());
                row.setValue((Attribute) fullDataSet.attribute(0), executeQuery.getInt("rowID"));
                row.setValue((Attribute) fullDataSet.attribute(1), executeQuery.getString("NP"));
                row.setValue((Attribute) fullDataSet.attribute(2), executeQuery.getString("Named_Entity_Class"));
                //System.out.println(executeQuery.getString("Pos"));
                row.setValue((Attribute) fullDataSet.attribute(3), executeQuery.getString("Pos"));
                row.setValue((Attribute) fullDataSet.attribute(4), executeQuery.getBoolean("hasRedLink") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(5), executeQuery.getBoolean("titleContainsNP") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(6), executeQuery.getBoolean("firstCapital") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(7), executeQuery.getBoolean("isAllCaps") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(8), executeQuery.getInt("dayOfWeek"));
                row.setValue((Attribute) fullDataSet.attribute(9), executeQuery.getInt("lastFullHour"));
                row.setValue((Attribute) fullDataSet.attribute(10), executeQuery.getInt("pageView24hSum"));
                row.setValue((Attribute) fullDataSet.attribute(11), executeQuery.getDouble("pageViewSlope24hSlope"));
                row.setValue((Attribute) fullDataSet.attribute(12), executeQuery.getDouble("pageViewSlope24hIntercept"));
                row.setValue((Attribute) fullDataSet.attribute(13), executeQuery.getInt("npOccurrenceNo1h"));
                row.setValue((Attribute) fullDataSet.attribute(14), executeQuery.getInt("npOccurrenceNo24h"));
                row.setValue((Attribute) fullDataSet.attribute(15), executeQuery.getDouble("npOccurrenceSlope24hSlope"));
                row.setValue((Attribute) fullDataSet.attribute(16), executeQuery.getDouble("npOccurrenceSlope24hIntercept"));
                row.setValue((Attribute) fullDataSet.attribute(17), executeQuery.getLong("googleNgramFrequency"));
                row.setValue((Attribute) fullDataSet.attribute(18), executeQuery.getDouble("npPositionInArticle"));
                row.setValue((Attribute) fullDataSet.attribute(19), executeQuery.getDouble("articleCoverage"));
                row.setValue((Attribute) fullDataSet.attribute(20), executeQuery.getInt("class"));
                //int classInt = executeQuery.getInt("class");
                //boolean boolClass = classInt == 1;
                //row.setValue((Attribute) fullDataSet.attribute(20), boolClass ? (1) : (0));
                row.setWeight(1);
                fullDataSet.add(row);
                trues++;
            }

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            Class.forName("org.sqlite.JDBC").newInstance();
            conn = DriverManager.getConnection(SQLITE_DB_URL);
            String stmntString = "SELECT rowID, NP, Named_Entity_Class, Pos, hasRedLink, "
                    + "titleContainsNP, firstCapital, isAllCaps, dayOfWeek, lastFullHour, pageView24hSum, pageViewSlope24hSlope, pageViewSlope24hIntercept, "
                    + "npOccurrenceNo1h, npOccurrenceNo24h, npOccurrenceSlope24hSlope, npOccurrenceSlope24hIntercept, googleNgramFrequency, npPositionInArticle, "
                    + "articleCoverage, class FROM allNPFeatures WHERE (class = 0) AND ((NPTimestamp BETWEEN  strftime('%s', '2015-04-24') AND  strftime('%s', '2015-04-25'))\n"
                    + "OR (NPTimestamp BETWEEN  strftime('%s', '2015-04-26') AND  strftime('%s', '2015-05-04'))) ORDER BY RANDOM() LIMIT 250000";
            Statement createStatement = conn.createStatement();
            ResultSet executeQuery = createStatement.executeQuery(stmntString);
            while (executeQuery.next()) {
                Instance row = new DenseInstance(fullDataSet.numAttributes());
                row.setValue((Attribute) fullDataSet.attribute(0), executeQuery.getInt("rowID"));
                row.setValue((Attribute) fullDataSet.attribute(1), executeQuery.getString("NP"));
                row.setValue((Attribute) fullDataSet.attribute(2), executeQuery.getString("Named_Entity_Class"));
                //System.out.println(executeQuery.getString("Pos"));
                row.setValue((Attribute) fullDataSet.attribute(3), executeQuery.getString("Pos"));
                row.setValue((Attribute) fullDataSet.attribute(4), executeQuery.getBoolean("hasRedLink") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(5), executeQuery.getBoolean("titleContainsNP") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(6), executeQuery.getBoolean("firstCapital") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(7), executeQuery.getBoolean("isAllCaps") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(8), executeQuery.getInt("dayOfWeek"));
                row.setValue((Attribute) fullDataSet.attribute(9), executeQuery.getInt("lastFullHour"));
                row.setValue((Attribute) fullDataSet.attribute(10), executeQuery.getInt("pageView24hSum"));
                row.setValue((Attribute) fullDataSet.attribute(11), executeQuery.getDouble("pageViewSlope24hSlope"));
                row.setValue((Attribute) fullDataSet.attribute(12), executeQuery.getDouble("pageViewSlope24hIntercept"));
                row.setValue((Attribute) fullDataSet.attribute(13), executeQuery.getInt("npOccurrenceNo1h"));
                row.setValue((Attribute) fullDataSet.attribute(14), executeQuery.getInt("npOccurrenceNo24h"));
                row.setValue((Attribute) fullDataSet.attribute(15), executeQuery.getDouble("npOccurrenceSlope24hSlope"));
                row.setValue((Attribute) fullDataSet.attribute(16), executeQuery.getDouble("npOccurrenceSlope24hIntercept"));
                row.setValue((Attribute) fullDataSet.attribute(17), executeQuery.getLong("googleNgramFrequency"));
                row.setValue((Attribute) fullDataSet.attribute(18), executeQuery.getDouble("npPositionInArticle"));
                row.setValue((Attribute) fullDataSet.attribute(19), executeQuery.getDouble("articleCoverage"));
                row.setValue((Attribute) fullDataSet.attribute(20), executeQuery.getInt("class"));
                //int classInt = executeQuery.getInt("class");
                //boolean boolClass = classInt == 1;
                //row.setValue((Attribute) fullDataSet.attribute(20), boolClass ? (1) : (0));
                row.setWeight(0.4);
                fullDataSet.add(row);
                falses++;
            }

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        int[] noClasses = SODEETrainerUtils.calcNumberInstancesOfClass(fullDataSet);
        trues = noClasses[0];
        falses = noClasses[1];

        sb.append("No. Instances in Dataset: ").append(fullDataSet.numInstances()).append("\n");
        sb.append("No. of trues in Dataset: ").append(trues).append("\n");
        sb.append("No. of falses in Dataset: ").append(falses).append("\n");
        sb.append("Distribution true: ").append(((trues / (trues + falses)) * 100)).append("\n");
        sb.append("Distribution false: ").append(((falses / (trues + falses)) * 100)).append("\n\n");

        int datasetSize = fullDataSet.numInstances();
        int trainingSetCardinality = Math.round(datasetSize * trainTestSplitValue);
        int testSetCardinality = datasetSize - trainingSetCardinality;
        fullDataSet.randomize(new Random(1));
        Instances trainingData;
        Instances testData;
        trainingData = new Instances(fullDataSet, 0, trainingSetCardinality);
        trainingData.setClassIndex(fullDataSet.numAttributes() - 1);
        trainingData.setRelationName("Training Dataset");
        testData = new Instances(fullDataSet, trainingSetCardinality, testSetCardinality);
        testData.setClassIndex(fullDataSet.numAttributes() - 1);
        testData.setRelationName("Test Dataset");

        int[] noClassesTrain = SODEETrainerUtils.calcNumberInstancesOfClass(trainingData);
        double truesTraining = noClassesTrain[0];
        double falsesTraining = noClassesTrain[1];

        sb.append("No. Instances in Training Dataset: ").append(trainingData.numInstances()).append("\n");
        sb.append("No. of trues in Training Dataset: ").append(truesTraining).append("\n");
        sb.append("No. of falses in Training Dataset: ").append(falsesTraining).append("\n");
        sb.append("Distribution true: ").append((truesTraining / (truesTraining + falsesTraining)) * 100).append("\n");
        sb.append("Distribution false: ").append((falsesTraining / (truesTraining + falsesTraining)) * 100).append("\n\n");

        int[] noClassesTest = SODEETrainerUtils.calcNumberInstancesOfClass(testData);
        double truesTest = noClassesTest[0];
        double falsesTest = noClassesTest[1];

        sb.append("No. Instances in Test Dataset: ").append(testData.numInstances()).append("\n");
        sb.append("No. of trues in Test Dataset: ").append(truesTest).append("\n");
        sb.append("No. of falses in Test Dataset: ").append(falsesTest).append("\n");
        sb.append("Distribution true: ").append((truesTest / (truesTest + falsesTest)) * 100).append("\n");
        sb.append("Distribution false: ").append((falsesTest / (truesTest + falsesTest)) * 100).append("\n\n");

        //Save Dataset
        ArffSaver arffWriter = new ArffSaver();
        arffWriter.setInstances(fullDataSet);
        String fullDataSetFile = outputPath + "full.arff";
        try {
            arffWriter.setFile(new File(fullDataSetFile));
            arffWriter.writeBatch();
        } catch (IOException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Save training data
        arffWriter = new ArffSaver();
        arffWriter.setInstances(trainingData);
        String trainingDataSetFile = outputPath + "training.arff";
        try {
            arffWriter.setFile(new File(trainingDataSetFile));
            arffWriter.writeBatch();
        } catch (IOException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Save test data
        arffWriter = new ArffSaver();
        arffWriter.setInstances(testData);
        String testDataSetFile = outputPath + "test.arff";
        try {
            arffWriter.setFile(new File(testDataSetFile));
            arffWriter.writeBatch();
        } catch (IOException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Set filter
        Remove rm = new Remove();
        //String[] removeFilterOptions = {"-R", "1-2"};
        String[] removeFilterOptions = {"-R", "1,2,3,8,9,10"};
        try {
            rm.setOptions(removeFilterOptions);
        } catch (Exception ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        Filter[] allFilters = new Filter[1];
        allFilters[0] = rm;

        MultiFilter filters = new MultiFilter();
        filters.setFilters(allFilters);

        //Uncomment for getting feature selection help
        /*
         AttributeSelection attrSelect = new AttributeSelection();
         InfoGainAttributeEval attrEvalInfoGain = new InfoGainAttributeEval();
         GainRatioAttributeEval attrEvalGainRatio = new GainRatioAttributeEval();
         CorrelationAttributeEval attrEvalCorr = new CorrelationAttributeEval();
         Ranker ranking = new Ranker();
         try {
         rm.setInputFormat(trainingData);
         Instances filteredData = Filter.useFilter(trainingData, rm);
         filteredData.setClassIndex(filteredData.numAttributes() - 1);
         attrEvalInfoGain.buildEvaluator(filteredData);
         attrSelect.setEvaluator(attrEvalInfoGain);
         attrSelect.setSearch(ranking);
         attrSelect.SelectAttributes(filteredData);
         //sb.append("\n").append(attrSelect.toResultsString()).append("\n");

         // Gain Ratio
         attrEvalGainRatio.buildEvaluator(filteredData);
         attrSelect.setEvaluator(attrEvalGainRatio);
         attrSelect.setSearch(ranking);
         attrSelect.SelectAttributes(filteredData);
         //sb.append("\n").append(attrSelect.toResultsString()).append("\n");

         // Correlation
         attrEvalCorr.buildEvaluator(filteredData);
         attrSelect.setEvaluator(attrEvalCorr);
         attrSelect.setSearch(ranking);
         attrSelect.SelectAttributes(filteredData);
         sb.append("\n").append(attrSelect.toResultsString()).append("\n");

         } catch (Exception ex) {
         Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
         }
         */
        SODEETrainerUtils.writeTXTFile(sb.toString(), outputPath + "datasetInfo.txt");

        // SVM CLASSIFIER
        SMO svmClassifier = new SMO();
        Puk pukKernel = new Puk();
        pukKernel.setOmega(4);
        pukKernel.setSigma(1);
        String svmName = "SVM_01";
        String svmDescription = "SVM with Puk, Sigma = 1, Omega = 4";
        try {
            svmClassifier.setKernel(pukKernel);
            svmClassifier.setBuildCalibrationModels(true); // fitting => for a better generating of ROC-Curve
        } catch (Exception ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        FilteredClassifier fcSVM01 = new FilteredClassifier();
        fcSVM01.setFilter(filters);
        fcSVM01.setClassifier(svmClassifier);

        Thread svmTrainer01 = new Thread(new SingleModelTrainer(trainingData, testData,
                svmName, svmDescription, outputPath, fcSVM01));
        svmTrainer01.setName(svmName);
        svmTrainer01.start();

    }

}
